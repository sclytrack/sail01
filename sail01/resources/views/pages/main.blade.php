@extends('layouts.mainlayout')


@section('content')

<h1>
    {{ config('app.name', 'sail01 app name error') }}
</h1>

<p class='rood'>
    This project is my first devcontainer with laravel sail.
</p>


<p>
    Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae vel quia blanditiis, fuga ipsa numquam eum unde dicta voluptatibus nihil id odit beatae minima, repellat cum sunt maxime assumenda eveniet.
</p>


<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <p>Modal body text goes here.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

@endsection()

