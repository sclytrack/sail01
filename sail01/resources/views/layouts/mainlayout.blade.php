<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'Call Peter for help.') }}</title>
    <script src="{{ URL::to('js/jquery-3.6.1.min.js') }}"></script>
</head>
    @vite(['resources/js/app.js'])
<body>

@include('include.heading')

@yield('content')

</body>
</html>